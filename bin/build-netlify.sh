#!/bin/bash
set -ex

gulp build --production
hugo hugo --config config.toml,config-prod.toml
